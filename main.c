//gcc -Wall -Wextra -Wshadow -Werror=implicit-function-declaration -std=c11 -g `pkg-config --cflags --libs gtk+-3.0` -lm -lpthread -lhandy-1 -lgtop-2.0 main.c graph.c graph_util.c resource_interface.c `pkg-config --libs glib-2.0` -I./ -I/usr/include/libgtop-2.0 -o main && ./main


#include <gtk/gtk.h>
#include <libhandy-1/handy.h>

#include <cairo.h>
#include <glib.h>
#include <math.h>
#include <pthread.h>

#include "resource_interface.h"

int main(int argc, char** argv)
{
    gtk_init(&argc, &argv);


    HdyApplicationWindow *main_window;
    GtkBuilder *builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "interface_new.ui", NULL);
    main_window = HDY_APPLICATION_WINDOW (gtk_builder_get_object (builder, "main_window"));





    start_resource_interface (main_window, builder);






    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(builder);

    gtk_widget_show(GTK_WIDGET(main_window));
    gtk_main();
    exit (0);
}