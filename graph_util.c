/* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <math.h>
#include <cairo.h>
#include <gtk/gtk.h>

#include "graph_util.h"

void
prefill (struct Resource_data_t *data, guint n) 
{
	guint i;

	for (i = 0; i < X_DATA_COL; i++) 
    {
        data->limit [i].max_life = X_RESOLUTION;
    }

	data->raw [0][0] = 0.0f;
    for (i = 1; i < n; i++)
    {
        data->raw [0][i] = (i % X_RESOLUTION)*1.0f/X_RESOLUTION;
    }
	data->matrix.x = 1;
	data->matrix.y = 1;
}

void
prefill_all (struct Graph_data_t *graph) 
{
	prefill (&graph->total, X_RESOLUTION);
    prefill (&graph->nphysical, graph->nphysical.ngraph*X_RESOLUTION);
	prefill (&graph->nlogical, graph->nlogical.ngraph*X_RESOLUTION);
}

void
clear_data (struct Resource_data_t *data, guint n) 
{
    memset(data, 0, sizeof(struct Resource_data_t));
}

void
initialize_resource (struct Graph_data_t *graph) 
{
    clear_data (&graph->total, graph->total.ngraph);
    clear_data (&graph->nphysical, graph->nphysical.ngraph);
	clear_data (&graph->nlogical, graph->nlogical.ngraph);
}

gboolean
is_prime (guint max, guint est, guint *i)
{

	if (max == 2 || max == 3 || max == 5 || max == 7) 
	{
		return TRUE;
	}
	for (*i = est; *i <= max; ++*i)
	{
		if (max % *i == 0)
		{	
			if (max/(*i) > 1)
			{
				return FALSE;
			}

		}
	}
	return TRUE;
}

/* Near-square algorithm */
void
get_multi_graph_dim (guint *x, guint *y, guint n)
{
	gdouble rough_x_count = sqrt (n);
	guint est_x_matrix = (int) rough_x_count, i, j;

	/* If divisble, get the most symmetric configuration */
	/* else if not divisible, get the closest symmetric configuration */
	if (rough_x_count == est_x_matrix)
	{
		*x = est_x_matrix;
		*y = est_x_matrix;
		return;
	}
	else if (!is_prime (n, est_x_matrix, &i))
	{
		*x = i;
		*y = n/i;
		
		if (i < n/i) 
		{
			*x = *y;
			*y = i;
		}
	}
	else 
	{

		for (i = est_x_matrix; i <= n; i++)
		{
			j = ceil ((float) n/i);
			if (i > j)
			{
				*x = i;
				*y = j;
				break;
			}
		}
	}
}

/*

    Cairo canvas default coordinate

    0,0     1,0

    0,1     1,1

*/

void
layout_coord (gfloat *dest, gfloat origin, guint dim, guint offset) 
{
    *dest = origin/dim + ((1.0f / dim) * offset);
	//printf ("%d\n", offset);
}



/* Generate cairo drawing data*/
/* modulo of 0 is undefined so shift data in the for loop but recover the index in layout_coord */
void
layout_plot (struct Resource_data_t *data, guint length, guint stats)
{
    guint i, j = 1, k;

    k = data->matrix.y - 1;

	layout_coord (&data->prep [0][0], 
				data->raw [0][0], 
				data->matrix.x,
				j - 1);
	/* y-axis */
	layout_coord (&data->prep [1][0], 
					data->raw [1][0], 
					data->matrix.y,
					k);

	if (stats & PAIR) 
	{
		layout_coord (&data->prep [2][0], 
						data->raw [2][0], 
						data->matrix.y,
						k);
	}

	if (stats & LATENCY) 
	{
		layout_coord (&data->prep [3][0], 
						data->raw [3][0], 
						data->matrix.y,
						k);
	}

	if (stats & TEMP) 
	{
		layout_coord (&data->prep [4][0], 
						data->raw [4][0], 
						data->matrix.y,
						k);
	}

    for (i = 1; i < length; i++) 
	{
		if (i % (X_RESOLUTION) == 0 && j % (data->matrix.x + 1) != 0) 
		{
			j++;
		}
		else if (j % (data->matrix.x + 1) == 0) 
		{
			j = 1;
			k--;
		}
		/* x-axis */
		layout_coord (&data->prep [0][i], 
						data->raw [0][i], 
						data->matrix.x,
						j - 1);

		/* y-axis */
		layout_coord (&data->prep [1][i], 
						data->raw [1][i], 
						data->matrix.y,
						k);

		//printf ("%d\n", data->matrix.y);
		//printf("Ram: %f\n", data->prep [1][i]);
		if (stats & PAIR) 
		{
			layout_coord (&data->prep [2][i], 
							data->raw [2][i], 
							data->matrix.y,
							k);
		}

		if (stats & LATENCY) 
		{
			layout_coord (&data->prep [3][i], 
							data->raw [3][i], 
							data->matrix.y,
							k);
		}

		if (stats & TEMP) 
		{
			layout_coord (&data->prep [4][i], 
							data->raw [4][i], 
							data->matrix.y,
							k);
		}

	}
}

void
cairo_data (struct Resource_data_t *data, guint length, guint stats) 
{
	guint i;
	for (i = 0; i < length; i++)
	{
		data->cairo [0][i] = data->prep [0][i];
		data->cairo [1][i] = data->prep [1][i];

		if (stats & PAIR) 
		{
			data->cairo [2][i] = data->prep [2][i];
		}

		if (stats & LATENCY) 
		{
			data->cairo [3][i] = data->prep [3][i];
		}

		if (stats & TEMP) 
		{
			data->cairo [4][i] = data->prep [4][i];
		}
	}
}


void
rescale_data (gfloat *arr, guint lenght, gfloat scaling_factor) 
{
    int i;
    
    for (i = 0; i < lenght; i++)
    {
        arr [i] *= scaling_factor; /* The scaling factor will max the data into 1 only */
    }
}

void
rescale_max (gfloat *arr, guint lenght, gfloat data, guint offset, gfloat min, gfloat *max, gint *max_life, gfloat overflow_factor) 
{
    guint i;
    gfloat new_max = 0;

    /* Check if the new data is a max */
    /* If the old max is gone, a new max from among existing data should be elected */

    --*max_life;

    if (data > *max) 
    {
        new_max = data * overflow_factor;

        rescale_data (&*arr, lenght, *max / new_max);

        *max_life = X_RESOLUTION - 1;
        *max = new_max;
    }
    else if (*max_life < 0)
    {
        guint new_max_life = 0;

		offset = (offset > 0) ? offset : 1;

        for (i = offset; i < lenght; i++)
        {
            if (arr [i] > new_max) 
            {
                new_max_life = i % X_RESOLUTION;
                new_max = arr [i];
            }
        }

        new_max = (*max) * new_max * overflow_factor;

        if (new_max < min) 
        {
            new_max = min;
        }

        rescale_data (&*arr, lenght, *max / new_max);

        *max_life = new_max_life;
        *max = new_max;
    }
}

void
shift (gfloat *arr, guint offset, guint x_end) 
{
    guint i;

    for (i = offset; i < x_end; i++) 
    {
        arr [i] = arr [i + 1];
    }
	//printf("%f\n", arr [90]);
}

void
add_data (gfloat *arr, gfloat data, guint offset, guint x_end)
{
    shift (&*arr, offset, x_end);
    arr [x_end - 1] = data;

}